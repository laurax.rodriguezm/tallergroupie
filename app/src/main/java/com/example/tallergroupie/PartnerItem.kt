package com.example.tallergroupie

import com.example.tallergroupie.databinding.ItemPartnerBinding
import com.xwray.groupie.databinding.BindableItem

class PartnerItem (val name: String, val code: String): BindableItem<ItemPartnerBinding>() {
    override fun bind(viewBinding: ItemPartnerBinding, position: Int) {
        viewBinding.nameTextView.text = name
        viewBinding.codeTextView.text = code
    }

    override fun getLayout(): Int {
        return R.layout.item_partner
    }
}