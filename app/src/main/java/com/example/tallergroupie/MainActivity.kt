package com.example.tallergroupie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tallergroupie.databinding.ActivityMainBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.GroupieAdapter

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)

        binding.partnersRecyclerView.layoutManager = LinearLayoutManager(this)

        val list = ExpandableGroup(GroupieStudent())
        list.add(PartnerItem("Laura Ximena Rodriguez Machado", "57955"))
        list.add(PartnerItem("Yorladis Grisales Henao", "57335"))
        list.add(PartnerItem("Yamile Yohana Lagares Arroyo", "55555"))

        val adapter = GroupieAdapter()
        binding.partnersRecyclerView.adapter = adapter
        adapter.add(list)



    }
}