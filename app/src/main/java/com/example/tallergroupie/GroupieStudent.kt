package com.example.tallergroupie

import com.example.tallergroupie.databinding.ItemPartnerBinding
import com.example.tallergroupie.databinding.StudentGroupieBinding
import com.xwray.groupie.ExpandableGroup
import com.xwray.groupie.ExpandableItem
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.databinding.BindableItem

class GroupieStudent: BindableItem<StudentGroupieBinding>(), ExpandableItem {
    lateinit var expandibleGroupie: ExpandableGroup

    override fun bind(viewBinding: StudentGroupieBinding, position: Int) {
        viewBinding.detailsButton.setOnClickListener{
            expandibleGroupie.onToggleExpanded()

        }

    }

    override fun getLayout(): Int {
        return R.layout.student_groupie
    }

    override fun setExpandableGroup(onToggleListener: ExpandableGroup) {
        this.expandibleGroupie= onToggleListener
    }

}